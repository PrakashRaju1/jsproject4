const defaults=(obj,defaultProps)=>{
    if(!obj || typeof obj!=="object") return {};
    if(!defaultProps || typeof defaultProps !== "object") return obj;
    for (let i in defaultProps){
        if(!obj[i]){
            obj[i]=defaultProps[i];
        }
    }
    return obj;
}

module.exports=defaults;