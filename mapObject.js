const cb = require("./test/testMapObject");
const mapObject =(obj,cb)=>{
    if(!obj || !cb) return {};
    const newObj={};
    for (let i in obj){
        newObj[i]=cb(obj[i]);
        
    }
return newObj;
}


module.exports=mapObject;